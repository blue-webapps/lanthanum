import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import webpack from 'webpack';
import { merge } from 'webpack-merge';

import common from './webpack.common.js';

const { DefinePlugin } = webpack;

export default merge(common, {
  mode: 'production',
  optimization: { minimize: true },
  plugins: [
    new DefinePlugin({ PRODUCTION: JSON.stringify(true) }),
    new MiniCssExtractPlugin({
      filename: '[name].[chunkhash].css',
      chunkFilename: '[id].[chunkhash].css',
    }),
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{ loader: MiniCssExtractPlugin.loader }, 'css-loader'],
      },
    ],
  },
});

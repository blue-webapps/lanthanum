/* eslint-disable import/no-nodejs-modules */

import assert from 'assert';
import { readFileSync } from 'fs';
import { merge } from 'webpack-merge';

import common from './webpack.common.js';

const { devPort } = JSON.parse(readFileSync('./package.json'));
assert(devPort, 'No devPort specified in package.json');

export default merge(common, {
  devtool: 'inline-source-map',
  mode: 'development',
  devServer: {
    port: devPort,
    historyApiFallback: true,
    host: '0.0.0.0',
  },
  watchOptions: { aggregateTimeout: 300 },

  plugins: [],

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
});

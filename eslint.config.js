import js from '@eslint/js';
import prettierConfig from 'eslint-config-prettier';
import importPlugin from 'eslint-plugin-import';
import preferArrowFunctions from 'eslint-plugin-prefer-arrow-functions';
import prettier from 'eslint-plugin-prettier';
import reactPlugin from 'eslint-plugin-react';
import hooksPlugin from 'eslint-plugin-react-hooks';
import simpleImportSort from 'eslint-plugin-simple-import-sort';
import globals from 'globals';

export default [
  js.configs.recommended,
  prettierConfig,
  reactPlugin.configs.flat.recommended,
  importPlugin.flatConfigs.recommended,
  {
    settings: {
      react: {
        version: 'detect',
      },
    },
  },
  {
    plugins: {
      'react-hooks': hooksPlugin,
    },
    rules: hooksPlugin.configs.recommended.rules,
  },
  {
    files: ['eslint.config.js', 'src/**/*.js', 'webpack.*.js', 'release.config.js'],

    plugins: {
      prettier,
      'simple-import-sort': simpleImportSort,
      'prefer-arrow-functions': preferArrowFunctions,
    },

    languageOptions: {
      globals: {
        ...globals.browser,
      },
      ecmaVersion: 2022,
      sourceType: 'module',
      parserOptions: {},
    },

    rules: {
      'arrow-body-style': ['error', 'as-needed'],
      'comma-dangle': ['error', 'always-multiline'],
      'max-len': ['error', 120],
      'no-restricted-syntax': 'off',

      'import/extensions': 'off',
      'import/no-extraneous-dependencies': 'off',
      'import/no-dynamic-require': 'warn',
      'import/no-nodejs-modules': 'warn',
      'import/no-named-as-default-member': 'off',

      'sort-imports': 'off',
      'simple-import-sort/imports': 'error',

      'react/display-name': 2,
      'react/function-component-definition': [2, { namedComponents: 'arrow-function' }],
      'react/prefer-stateless-function': 2,
      'react/prop-types': 'off',

      'react-hooks/exhaustive-deps': 'off',
      'react-hooks/rules-of-hooks': 'error',

      'prettier/prettier': [
        'error',
        {
          singleQuote: true,
          printWidth: 120,
          trailingComma: 'all',
        },
      ],

      'prefer-arrow-functions/prefer-arrow-functions': [
        'warn',
        {
          allowNamedFunctions: false,
          classPropertiesAllowed: false,
          disallowPrototype: false,
          returnStyle: 'unchanged',
          singleReturnOnly: false,
        },
      ],
    },
  },
];

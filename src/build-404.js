/* eslint-disable import/no-nodejs-modules */
import ejs from 'ejs';
import fs from 'fs';
import process from 'process';

const template = `
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
</head>

<body>
  <script type="text/javascript">
    window.location.replace("<%= BASE_PATH %>");
  </script>
</body>

</html>
`;

const { BASE_PATH } = process.env;

const res = ejs.render(template, { BASE_PATH });

fs.writeFileSync('public/404.html', res);

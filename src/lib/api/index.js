import photos from './photos';
import users from './users';

const api = {
  users,
  photos,
};

export default api;
